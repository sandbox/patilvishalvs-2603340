<?php

namespace Drupal\image_class_8\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'image_class_8' formatter.
 *
 * @FieldFormatter(
 *   id = "image_class_8",
 *   label = @Translation("Image class"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageClassFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array('image_class_8' => '') + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['image_class_8'] = array(
      '#type' => 'textfield',
      '#title' => t('Image Class'),
      '#default_value' => $this->getSetting('image_class_8'),
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = t('Image Class: @class', array(
      '@class' => $this->getSetting('image_class_8'),
    ));
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as &$element) {
      $element['#item_attributes']['class'] = [
        $this->getSetting('image_class_8'),
      ];
    }

    return $elements;
  }

}
